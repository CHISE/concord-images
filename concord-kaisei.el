(concord-assign-genre 'glyph-image "/usr/local/var/photo/db")

(concord-assign-genre 'hng-card "/usr/local/var/hng-card/db")
(mount-char-attribute-table '->HNG)
(mount-char-attribute-table '<-HNG)

(defun concord-kaisei-add-takuhon (source-id file-prefix taku-num
					     source-name
					     source-short-name ccs)
  (let ((taku-file (format "%s%03d" file-prefix taku-num))
	glyph-num
	base-cobj
	base-id
	x y w h ucs seg-cobj char
	seg-glyph-cobj seg-glyph-id
	char-num-alist ret char-num ucs-hng-chars hng-code hng-cards)
    (setq base-cobj
	  (concord-images-add-url
	   (format
	    "http://hng.chise.org/images/zinbun/takuhon/kaisei/%s.jpg"
	    taku-file)
	   (format
	    "http://hng.chise.org/images/iiif/zinbun/takuhon/kaisei/%s.tif"
	    taku-file)
	   (format
	    "http://hng.chise.org/iipsrv/iipsrv.fcgi?FIF=/zinbun/takuhon/kaisei/%s.tif&CVT=jpeg"
	    taku-file)))
    (setq base-id (concord-object-get base-cobj '=id))
    (concord-object-put
     base-cobj 'name (format "%s%d" source-name taku-num))
    (concord-object-put
     base-cobj
     '=location@djvu
     (format
      "http://coe21.zinbun.kyoto-u.ac.jp/db-machine/imgsrv/djvu/kaisei/%s.djvu"
      taku-file))
    (with-current-buffer
	(find-file-noselect
	 (format "/opt/photo/zinbun/djvuchar/takuhonkaisei/%s.csv"
		 taku-file))
      (goto-char (point-min))
      (setq glyph-num 0)
      (while (re-search-forward
	      "^\\([0-9]+\\),\\([0-9]+\\),\\([0-9]+\\),\\([0-9]+\\),\\([0-9]+\\)"
	      nil t)
	(setq x (string-to-int (match-string 1))
	      y (string-to-int (match-string 2))
	      w (string-to-int (match-string 3))
	      h (string-to-int (match-string 4))
	      ucs (string-to-int (match-string 5)))
	(setq seg-cobj (concord-images-add-segments base-cobj x y w h))
	(setq seg-glyph-id (intern (format "%s/char=%d" base-id glyph-num)))
	(unless (setq seg-glyph-cobj
		      (concord-decode-object '=id seg-glyph-id))
	  (setq seg-glyph-cobj
		(concord-make-object 'glyph-image seg-glyph-id))
	  (concord-object-adjoin*
	   seg-glyph-cobj '->image-resource seg-cobj)
	  (concord-object-adjoin*
	   seg-glyph-cobj '<-segmented-glyph-image base-cobj)
	  )
	(setq char (decode-char '=ucs ucs))
	(concord-object-put seg-glyph-cobj 'character (list char))
	(cond ((setq ret (assq char char-num-alist))
	       (setq char-num (1+ (cdr ret)))
	       (setcdr ret char-num)
	       )
	      (t
	       (setq char-num 0)
	       (setq char-num-alist (cons (cons char 0)
					  char-num-alist))
	       ))
	(concord-object-put
	 seg-glyph-cobj
	 'name (format "%s%d-%d (%c-%d)"
		       source-short-name taku-num glyph-num char char-num))
	(concord-object-put
	 seg-cobj
	 '=location@djvuchar
	 (format "http://coe21.zinbun.kyoto-u.ac.jp/djvuchar?jpg=data/takuhonkaisei/%s&coords=%d,%d,%d,%d"
		 taku-file x y (+ x w) (+ y h)))
	(setq ucs-hng-chars (append (char-feature char '->HNG)
				    (char-feature char '->HNG@zinbun/takuhon)))
	(setq hng-cards nil)
	(dolist (hng-char ucs-hng-chars)
	  (when (and (setq hng-code (encode-char hng-char ccs))
		     (setq ret (concord-decode-object
				'=hng-card
				(intern (format "%d-%d"
						source-id
						(/ hng-code 10)))
				'hng-card))
		     (not (member ret hng-cards)))
	    (setq hng-cards (append hng-cards (list ret)))))
	(concord-object-put seg-glyph-cobj
			    '<-glyph-image@zinbun/takuhon hng-cards)
	(setq glyph-num (1+ glyph-num))
	))))

(defun concord-kaisei-fix-buffer (buffer)
  (interactive "bBuffer ")
  (goto-char (point-min))
  (let (beg end code)
    (while (re-search-forward
	    ",38 35 120 \\(\\([0-9]+\\)\\( [0-9]+\\)+\\) 59$" nil t)
      (setq beg (1+ (match-beginning 0))
	    end (match-end 0))
      (setq code (string-to-int
		  (mapconcat
		   (lambda (unit)
		     (char-to-string
		      (decode-char 'ascii
				   (string-to-number unit))))
		   (split-string (match-string 1) " ")
		   "")
		  16))
      (delete-region beg end)
      (insert (format "%d" code)))))
